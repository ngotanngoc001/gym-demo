alter PROC CUSTOMER_SELECT --10
@cusId INT = 0
AS
	IF (@cusId = 0)
		SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS STT,
			   A.CustomerID,
			   A.Barcode,
			   A.FullName,
			   A.IdentityCard,
			   A.BornDay,
			   A.Phone,
			   A.[Address],
			   A.Email,
			   A.Height,
			   A.Width,
			   A.Note,
			   A.ImageCamera,
			   A.Sex,
			  (CASE 
					WHEN A.Sex = 0 THEN	N'Nữ'
					WHEN A.Sex = 1 THEN	N'Nam'
					ELSE N'Bê đê'
			   END) AS Sex_Show
		FROM dbo.CUSTOMER AS A
		WHERE A.Active = 1
	ELSE 
		SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS STT,
			   A.CustomerID,
			   A.Barcode,
			   A.FullName,
			   A.IdentityCard,
			   A.BornDay,
			   A.Phone,
			   A.[Address],
			   A.Email,
			   A.Height,
			   A.Width,
			   A.Note,
			   A.ImageCamera,
			   A.Sex,
			  (CASE 
					WHEN A.Sex = 0 THEN	N'Nữ'
					WHEN A.Sex = 1 THEN	N'Nam'
					ELSE N'Bê đê'
			   END) AS Sex_Show
		FROM dbo.CUSTOMER AS A
		WHERE A.Active = 1 AND A.CustomerID = @cusId
GO
ALTER PROC CUSTOMER_INSERT_UPDATE
@CusId INT = 0,
@Barcode VARCHAR(50),
@Fullname NVARCHAR(255),
@Card VARCHAR(50),
@BornDay DATE,
@Sex INT,
@Phone VARCHAR(20),
@Address NVARCHAR(MAX),
@Email NVARCHAR(50),
@Height VARCHAR(10),
@Width VARCHAR(10),
@Note NVARCHAR(max),
@Image IMAGE
AS
	IF(@CusId <> 0 and (SELECT 1 FROM dbo.CUSTOMER AS A WHERE a.Active = 1 AND A.CustomerID = @CusId) = 1)
		BEGIN
			UPDATE	dbo.CUSTOMER SET Barcode = @Barcode, FullName = @Fullname, IdentityCard = @Card, BornDay = @BornDay, Sex = @Sex,
									 Phone = @Phone, [Address] = @Address, Email = @Email, Height = @Height, Width = @Width, Note = @Note,
									 ImageCamera = @Image
			WHERE CustomerID = @CusId
			SELECT ErrorCode = @CusId , ErrorMessage = N'Thành công !'
		END
	ELSE 
	
	BEGIN
		INSERT INTO dbo.CUSTOMER
		        ( Barcode ,
		          FullName ,
		          IdentityCard ,
		          BornDay ,
		          Sex ,
		          Phone ,
		          Address ,
		          Email ,
		          Height ,
		          Width ,
		          Note ,
		          ImageCamera ,
		          Active
		        )
		VALUES  ( @Barcode , -- Barcode - varchar(50)
		          @Fullname, -- FullName - nvarchar(255)
		          @Card, -- IdentityCard - varchar(50)
		          @BornDay , -- BornDay - date
		          @Sex , -- Sex - bit
		          @Phone , -- Phone - varchar(20)
		          @Address, -- Address - nvarchar(max)
		          @Email, -- Email - nvarchar(50)
		          @Height, -- Height - varchar(10)
		          @Width, -- Width - varchar(10)
		          @Note , -- Note - nvarchar(max)
		          @Image, -- ImageCamera - image
		          1  -- Active - bit
		        )
	SELECT ErrorCode = @@IDENTITY, ErrorMessage = N'Thành công !'
	END
GO

alter PROC CUSTOMER_FINGER_INSERT
@cusId INT = 0,
@finger VARBINARY(MAX)
AS
	 
		IF(@CusId <> 0)
			BEGIN
				INSERT dbo.CUSTOMER_FINGERPRINT
				        (CustomerID, FingerPrint, Active )
				VALUES  ( @cusId, @finger,1)
				SELECT Errorcode = 1, ErrorMessage  = N'Thành công !'
			END
			
GO

ALTER  PROC CUSTOMER_FINGER_DEL
@cusId INT = 0
AS
	UPDATE dbo.CUSTOMER_FINGERPRINT SET Active = 0 WHERE CustomerID = @cusId
GO

alter PROC CUSTOMER_GETFINGER
@cusId INT = 0
AS
	IF(@cusId <> 0)
		SELECT * FROM dbo.CUSTOMER_FINGERPRINT AS A WHERE A.CustomerID = @cusId AND A.Active = 1
	ELSE 
		SELECT * FROM dbo.CUSTOMER_FINGERPRINT AS A WHERE A.Active = 1
GO

SELECT * FROM dbo.CUSTOMER
SELECT * FROM dbo.CUSTOMER_FINGERPRINT


SELECT * 
FROM dbo.CUSTOMER AS A JOIN dbo.CUSTOMER_FINGERPRINT AS B ON A.CustomerID = B.CustomerID
WHERE B.Active = 1

ALTER TABLE dbo.CUSTOMER_FINGERPRINT ALTER COLUMN FingerPrint VARBINARY(MAX)
DELETE dbo.CUSTOMER_FINGERPRINT