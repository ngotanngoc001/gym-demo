CREATE DATABASE GymsDemo
ON
( NAME = GymsDemo_dat,
    FILENAME = 'E:\NgoTanNgoc\MyDatabase\gym.mdf',
    SIZE = 10MB,
    MAXSIZE = 50MB,
    FILEGROWTH = 5MB )  
LOG ON
( NAME = GymsDemo_log,
    FILENAME = 'E:\NgoTanNgoc\MyDatabase\gym_log.ldf',
    SIZE = 5MB,
    MAXSIZE = 25MB,
    FILEGROWTH = 5MB );
GO
USE GymsDemo
GO

CREATE TABLE CUSTOMER
(
	CustomerID INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	Barcode VARCHAR(50) NULL,
	FullName NVARCHAR(255) NOT NULL,
	IdentityCard VARCHAR(50) NULL,
	BornDay DATE NULL,
	Sex INT DEFAULT (0) NOT NULL,
	Phone VARCHAR(20) NULL,
	[Address] NVARCHAR(max) NULL,
	Email NVARCHAR(50) NULL,
	Height VARCHAR(10) NULL,
	Width VARCHAR(10) NULL,
	Note NVARCHAR(max) NULL,
	ImageCamera IMAGE NULL,
	Active BIT DEFAULT(1) NOT NULL
)
ALTER TABLE dbo.CUSTOMER ALTER COLUMN ImageCamera IMAGE
GO
CREATE TABLE CUSTOMER_FINGERPRINT
(
	FingerID INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	CustomerID INT NOT NULL,
	FingerImage_1 IMAGE NOT NULL,
	FingerImage_2 IMAGE NOT NULL,
	Quantity_1 INT NOT NULL,
	Quantity_2 INT NOT NULL,
	Finger_1 VARBINARY(MAX) NULL,
	Finger_2 VARBINARY(MAX) NULL,
	Active BIT DEFAULT(1) NOT NULL
	
	FOREIGN KEY (CustomerID) REFERENCES dbo.CUSTOMER (CustomerID)
)
GO

INSERT INTO dbo.CUSTOMER
        ( Barcode ,
          FullName ,
          IdentityCard ,
          BornDay ,
          Sex ,
          Phone ,
          Address ,
          Email ,
          Height ,
          Width ,
          Note ,
          ImageCamera ,
          Active
        )
VALUES  ( '123456789' , -- Barcode - varchar(50)
          N'Ngô Tấn ngọc' , -- FullName - nvarchar(255)
          '215401393' , -- IdentityCard - varchar(50)
          CONVERT(DATE, '1996-12-14'), -- BornDay - date
          1 , -- Sex - bit
          '0961599434' , -- Phone - varchar(20)
          N'Bình Định' , -- Address - nvarchar(max)
          N'ngotanngoc002@gmail.com' , -- Email - nvarchar(50)
          '1m62' , -- Height - varchar(10)
          '50kg' , -- Width - varchar(10)
          N'không' , -- Note - nvarchar(max)
          NULL , -- ImageCamera - image
          1  -- Active - bit
        )
