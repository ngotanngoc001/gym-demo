﻿using Common.Core.Data;
using SecuGen.FDxSDKPro.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gyms_demo
{
    public partial class frm_KhachHang : Form
    {
        public frm_KhachHang()
        {
            InitializeComponent();
        }
        private SGFingerPrintManager m_FPM;
        private SGFPMSecurityLevel m_SecurityLevel;

        DataTable save_finger;
        int count_finger = 1;
        int remain_click = 0;
        int type_image = 0;
        private bool m_LedOn = false;
        private Int32 m_ImageWidth;
        private Int32 m_ImageHeight;
        private Byte[] m_RegMin1;
        private Byte[] m_RegMin2;
        private Byte[] m_VrfMin;
        private SGFPMDeviceList[] m_DevList; // Used for EnumerateDevice
        int cusId = 0;
        int remain_cusId = 0;
        int sex = 0;

        private void frm_KhachHang_Load(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    DataTable dt = db.SqlQueryWithResult("[dbo].[CUSTOMER_SELECT]", CommandType.StoredProcedure);
                    if (dt.Rows.Count > 0)
                    {
                        btnInsert_Click(sender, e);
                        grCustomer.DataSource = dt;
                        GetInfo_Customer((int)dt.Rows[0]["CustomerID"]);
                        GetFinger_Customer((int)dt.Rows[0]["CustomerID"]);
                    }
                    else
                    {
                        grCustomer.DataSource = null;
                    }
                    LoadDevice();
                    if (!string.IsNullOrEmpty(comboBoxDeviceName.Text))
                    {
                        btnConnectDevice_Click(sender, e);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }

        private void LoadDevice()
        {
            m_LedOn = false;

            m_RegMin1 = new Byte[400];
            m_RegMin2 = new Byte[400];
            m_VrfMin = new Byte[400];

            comboBoxSecuLevel_R.SelectedIndex = 4;
            comboBoxSecuLevel_V.SelectedIndex = 3;

            m_FPM = new SGFingerPrintManager();
            EnumerateBtn();
        }
        private void EnumerateBtn()
        {
            Int32 iError;
            string enum_device;

            comboBoxDeviceName.Items.Clear();

            // Enumerate Device
            iError = m_FPM.EnumerateDevice();

            // Get enumeration info into SGFPMDeviceList
            m_DevList = new SGFPMDeviceList[m_FPM.NumberOfDevice];

            for (int i = 0; i < m_FPM.NumberOfDevice; i++)
            {
                m_DevList[i] = new SGFPMDeviceList();
                m_FPM.GetEnumDeviceInfo(i, m_DevList[i]);
                enum_device = m_DevList[i].DevName.ToString() + " : " + m_DevList[i].DevID;
                comboBoxDeviceName.Items.Add(enum_device);
            }

            if (comboBoxDeviceName.Items.Count > 0)
            {
                // Add Auto Selection
                enum_device = "Chọn thiết bị";
                comboBoxDeviceName.Items.Add(enum_device);
                comboBoxDeviceName.SelectedIndex = 0;  //First selected one

            }

        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void gvCustomer_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            var row = gvCustomer.GetDataRow(e.RowHandle);
            if (row != null)
            {
                cusId = remain_cusId = int.Parse(row["CustomerID"].ToString());
                txtBarcode.Text = row["Barcode"].ToString();
                txtFullname.Text = row["FullName"].ToString();
                txtCard.Text = row["IdentityCard"].ToString();
                dateBorn.Text = row["BornDay"].ToString();
                SetSex(int.Parse(row["Sex"].ToString()));
                txtPhone.Text = row["Phone"].ToString();
                txtAddress.Text = row["Address"].ToString();
                txtEmail.Text = row["Email"].ToString();
                txtHeight.Text = row["Height"].ToString();
                txtWidth.Text = row["Width"].ToString();
                txtNote.Text = row["Note"].ToString();
                if (!string.IsNullOrEmpty(row["ImageCamera"].ToString()))
                {
                    var data = (Byte[])row["ImageCamera"];
                    pictureBox1.Image = byteArrayToImage(data);
                }
                else
                {
                    pictureBox1.Image = Properties.Resources.no_image;
                }
                pictureBoxR1.Image = null;
                pictureBoxR2.Image = null;
                GetFinger_Customer(cusId);
            }
        }

        private DataTable GetFinger_Customer(int cusId)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    DataTable dt = db.SqlQueryWithResult("[dbo].[CUSTOMER_GETFINGER]", CommandType.StoredProcedure, new SqlParameter("@cusId", cusId));
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (!string.IsNullOrEmpty(dr["FingerImage_1"].ToString()) && !string.IsNullOrEmpty(dr["FingerImage_2"].ToString()))
                            {
                                byte[] data_1 = (Byte[])dr["FingerImage_1"];
                                byte[] data_2 = (Byte[])dr["FingerImage_2"];
                                pictureBoxR1.Image = byteArrayToImage(data_1);
                                pictureBoxR2.Image = byteArrayToImage(data_2);
                                progressBar_R1.Value = (int)dr["Quantity_1"];
                                progressBar_R2.Value = (int)dr["Quantity_2"];
                            }
                        }
                    }
                    return save_finger = dt;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                    return null;
                }
            }
        }

        private void SetSex(int s)
        {
            if (s == 0)
            {
                radGirl.Checked = true;
            }
            else if (s == 1)
            {
                radMen.Checked = true;
            }
            else
            {
                radOther.Checked = true;
            }
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            cusId = 0;
            txtAddress.Text = "";
            txtBarcode.Text = "";
            txtCard.Text = "";
            txtEmail.Text = "";
            txtFullname.Text = "";
            txtHeight.Text = "";
            txtNote.Text = "";
            txtPhone.Text = "";
            txtWidth.Text = "";
        }

        private void radGirl_CheckedChanged(object sender, EventArgs e)
        {
            sex = 0;
        }

        private void radMen_CheckedChanged(object sender, EventArgs e)
        {
            sex = 1;
        }

        private void radOther_CheckedChanged(object sender, EventArgs e)
        {
            sex = 2;
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    if (comboBoxDeviceName.Text == "" || txtBarcode.Text == "" || txtFullname.Text == "" || txtPhone.Text == "")
                    {
                        MessageBox.Show("Vui vòng không bỏ những dòng quy định nhập !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (pictureBox1.Image == null)
                    {
                        pictureBox1.Image = Properties.Resources.no_image;
                    }
                    else
                    {
                        if (Save_infoCus(cusId, txtBarcode.Text, txtFullname.Text, txtCard.Text, dateBorn.Value.Date, sex, txtPhone.Text, txtAddress.Text, txtEmail.Text, txtHeight.Text, txtWidth.Text, txtNote.Text, pictureBox1.Image) == true && type_image == 2)
                        {
                            db.SqlQuery("[dbo].[CUSTOMER_FINGER_DEL]", CommandType.StoredProcedure, new SqlParameter("@cusId", cusId));
                            if (Save_Detail_InfoCus(save_finger, cusId) == false) { MessageBox.Show("Lỗi hệ thống !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error); };
                        }
                    }
                    frm_KhachHang_Load(sender, e);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }

        private bool Save_infoCus(int cusId, string barcode, string name, string card, DateTime date, int sex, string phone, string address, string email, string height, string width, string note, Image img)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    db.SqlQuery("[dbo].[CUSTOMER_FINGER_DEL]", CommandType.StoredProcedure, new SqlParameter("@cusId", cusId));
                    DataTable dt = db.SqlQueryWithResult("[dbo].[CUSTOMER_INSERT_UPDATE]", CommandType.StoredProcedure,
                        new SqlParameter("@CusId", cusId),
                        new SqlParameter("@Barcode", barcode),
                        new SqlParameter("@Fullname", name),
                        new SqlParameter("@Card", card),
                        new SqlParameter("@BornDay", date),
                        new SqlParameter("@Sex", sex),
                        new SqlParameter("@Phone", phone),
                        new SqlParameter("@Address", address),
                        new SqlParameter("@Email", email),
                        new SqlParameter("@Height", height),
                        new SqlParameter("@Width", width),
                        new SqlParameter("@Note", note),
                        new SqlParameter("@Image", imageToByteArray(img)));
                    if (!string.IsNullOrEmpty(dt.Rows[0]["ErrorCode"].ToString()))
                    {
                        remain_cusId = int.Parse(dt.Rows[0]["Errorcode"].ToString());
                        MessageBox.Show(dt.Rows[0]["ErrorMessage"].ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Lỗi hệ thống !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                    return false;
                }
            }
        }
        private bool Save_Detail_InfoCus(DataTable dt, int cusId)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    string err = "";
                    foreach (DataRow dr in save_finger.Rows)
                    {
                        dr["CustomerID"] = cusId.ToString();
                    }

                    bool bulk = db.BulkCopy(save_finger, "CUSTOMER_FINGERPRINT", ref err);
                    if (bulk != true) { return false; }
                    StatusBar.Text = "Đã đăng ký vân tay thành công !";
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                    return false;
                }
            }
        }

        private void btnConnectDevice_Click(object sender, EventArgs e)
        {
            if (m_FPM.NumberOfDevice == 0)
                return;

            Int32 iError;
            SGFPMDeviceName device_name;
            Int32 device_id;

            Int32 numberOfDevices = comboBoxDeviceName.Items.Count;
            Int32 deviceSelected = comboBoxDeviceName.SelectedIndex;
            Boolean autoSelection = (deviceSelected == (numberOfDevices - 1));  // Last index

            if (autoSelection)
            {
                // Order of search: Hamster IV(HFDU04) -> Plus(HFDU03) -> III (HFDU02)
                device_name = SGFPMDeviceName.DEV_AUTO;

                device_id = (Int32)(SGFPMPortAddr.USB_AUTO_DETECT);
            }
            else
            {
                device_name = m_DevList[deviceSelected].DevName;
                device_id = m_DevList[deviceSelected].DevID;
            }

            iError = m_FPM.Init(device_name);
            iError = m_FPM.OpenDevice(device_id);

            CheckBoxAutoOn.Enabled = false;
            if (iError == (Int32)SGFPMError.ERROR_NONE)
            {
                GetInfo();
                StatusBar.Text = "Kết nối thành công !";

                // FDU03, FDU04 or higher
                if (device_name >= SGFPMDeviceName.DEV_FDU03)
                    CheckBoxAutoOn.Enabled = true;
            }
            else
                DisplayError("Lỗi mở kết nối !", iError);
        }
        /// GetDeviceInfo()
        private void GetInfo()
        {
            SGFPMDeviceInfoParam pInfo = new SGFPMDeviceInfoParam();
            Int32 iError = m_FPM.GetDeviceInfo(pInfo);

            if (iError == (Int32)SGFPMError.ERROR_NONE)
            {
                m_ImageWidth = pInfo.ImageWidth;
                m_ImageHeight = pInfo.ImageHeight;

                string DeviceID = Convert.ToString(pInfo.DeviceID);
                string ImageDPI = Convert.ToString(pInfo.ImageDPI);
                string FWVersion = Convert.ToString(pInfo.FWVersion, 16);

                ASCIIEncoding encoding = new ASCIIEncoding();
                string SerialNum = encoding.GetString(pInfo.DeviceSN);

                string ImageHeight = Convert.ToString(pInfo.ImageHeight);
                string ImageWidth = Convert.ToString(pInfo.ImageWidth);
                string Brightness = Convert.ToString(pInfo.Brightness);
                string Contrast = Convert.ToString(pInfo.Contrast);
                string Gains = Convert.ToString(pInfo.Gain);

                NumericUpDown BrightnessUpDown = new NumericUpDown();
                BrightnessUpDown.Value = 70;
                BrightnessUpDown.Value = pInfo.Brightness;
            }
        }
        ///////////////////////
        void DisplayError(string funcName, int iError)
        {
            string text = "";

            switch (iError)
            {
                case 0:                             //SGFDX_ERROR_NONE				= 0,
                    text = "Error none";
                    break;

                case 1:                             //SGFDX_ERROR_CREATION_FAILED	= 1,
                    text = "Can not create object";
                    break;

                case 2:                             //   SGFDX_ERROR_FUNCTION_FAILED	= 2,
                    text = "Function Failed";
                    break;

                case 3:                             //   SGFDX_ERROR_INVALID_PARAM	= 3,
                    text = "Invalid Parameter";
                    break;

                case 4:                          //   SGFDX_ERROR_NOT_USED			= 4,
                    text = "Not used function";
                    break;

                case 5:                                //SGFDX_ERROR_DLLLOAD_FAILED	= 5,
                    text = "Can not create object";
                    break;

                case 6:                                //SGFDX_ERROR_DLLLOAD_FAILED_DRV	= 6,
                    text = "Can not load device driver";
                    break;
                case 7:                                //SGFDX_ERROR_DLLLOAD_FAILED_ALGO = 7,
                    text = "Can not load sgfpamx.dll";
                    break;

                case 51:                //SGFDX_ERROR_SYSLOAD_FAILED	   = 51,	// system file load fail
                    text = "Can not load driver kernel file";
                    break;

                case 52:                //SGFDX_ERROR_INITIALIZE_FAILED  = 52,   // chip initialize fail
                    text = "Failed to initialize the device";
                    break;

                case 53:                //SGFDX_ERROR_LINE_DROPPED		   = 53,   // image data drop
                    text = "Data transmission is not good";
                    break;

                case 54:                //SGFDX_ERROR_TIME_OUT			   = 54,   // getliveimage timeout error
                    text = "Time out";
                    break;

                case 55:                //SGFDX_ERROR_DEVICE_NOT_FOUND	= 55,   // device not found
                    text = "Device not found";
                    break;

                case 56:                //SGFDX_ERROR_DRVLOAD_FAILED	   = 56,   // dll file load fail
                    text = "Can not load driver file";
                    break;

                case 57:                //SGFDX_ERROR_WRONG_IMAGE		   = 57,   // wrong image
                    text = "Wrong Image";
                    break;

                case 58:                //SGFDX_ERROR_LACK_OF_BANDWIDTH  = 58,   // USB Bandwith Lack Error
                    text = "Lack of USB Bandwith";
                    break;

                case 59:                //SGFDX_ERROR_DEV_ALREADY_OPEN	= 59,   // Device Exclusive access Error
                    text = "Device is already opened";
                    break;

                case 60:                //SGFDX_ERROR_GETSN_FAILED		   = 60,   // Fail to get Device Serial Number
                    text = "Device serial number error";
                    break;

                case 61:                //SGFDX_ERROR_UNSUPPORTED_DEV		   = 61,   // Unsupported device
                    text = "Unsupported device";
                    break;

                // Extract & Verification error
                case 101:                //SGFDX_ERROR_FEAT_NUMBER		= 101, // utoo small number of minutiae
                    text = "The number of minutiae is too small";
                    break;

                case 102:                //SGFDX_ERROR_INVALID_TEMPLATE_TYPE		= 102, // wrong template type
                    text = "Template is invalid";
                    break;

                case 103:                //SGFDX_ERROR_INVALID_TEMPLATE1		= 103, // wrong template type
                    text = "1st template is invalid";
                    break;

                case 104:                //SGFDX_ERROR_INVALID_TEMPLATE2		= 104, // vwrong template type
                    text = "2nd template is invalid";
                    break;

                case 105:                //SGFDX_ERROR_EXTRACT_FAIL		= 105, // extraction fail
                    text = "Minutiae extraction failed";
                    break;

                case 106:                //SGFDX_ERROR_MATCH_FAIL		= 106, // matching  fail
                    text = "Matching failed";
                    break;

            }

            text = funcName + " Error # " + iError + " :" + text;
            StatusBar.Text = text;
        }

        private void btnFinger_Click(object sender, EventArgs e)
        {
            Byte[] fp_image = new Byte[m_ImageWidth * m_ImageHeight];
            type_image = 2;

            if (count_finger == 1)
            {
                pictureBoxR1_Load(fp_image, 0);
                count_finger = 2;
            }
            else
            {
                pictureBoxR2_Load(fp_image, 0);
                count_finger = 1;
            }
        }

        private DataTable CreatedTable(byte[] img_1, byte[] img_2, int quan_1, int quan_2, byte[] finger1, byte[] finger2)
        {
            save_finger = new DataTable();
            if (save_finger.Columns.Count < 1)
            {
                save_finger.Columns.Add("FingerID", typeof(int));
                save_finger.Columns.Add("CustomerID", typeof(int));
                save_finger.Columns.Add("FingerImage_1", typeof(byte[]));
                save_finger.Columns.Add("FingerImage_2", typeof(byte[]));
                save_finger.Columns.Add("Quantity_1", typeof(int));
                save_finger.Columns.Add("Quantity_2", typeof(int));
                save_finger.Columns.Add("Finger_1", typeof(byte[]));
                save_finger.Columns.Add("Finger_2", typeof(byte[]));
                save_finger.Columns.Add("Active", typeof(Boolean));

                save_finger.Rows.Add(new object[] { null, null, img_1, img_2, quan_1, quan_2, finger1, finger2, 1 });
            }
            else
            {
                save_finger.Rows.Add(new object[] { null, null, img_1, img_2, quan_1, quan_2, finger1, finger2, 1 });
            }
            return save_finger;
        }

        ///////////////////////
        private void pictureBoxR1_Load(Byte[] fp_image, Int32 img_qlty)
        {
            Int32 iError = 0;

            fp_image = new Byte[m_ImageWidth * m_ImageHeight];
            iError = m_FPM.GetImage(fp_image);

            m_FPM.GetImageQuality(m_ImageWidth, m_ImageHeight, fp_image, ref img_qlty);
            progressBar_R1.Value = img_qlty;

            if (iError == (Int32)SGFPMError.ERROR_NONE)
            {
                DrawImage(fp_image, pictureBoxR1);
                iError = m_FPM.CreateTemplate(fp_image, m_RegMin1);
                if (iError == (Int32)SGFPMError.ERROR_NONE)
                {
                    StatusBar.Text = "Chụp hình: Vân tay 1";
                }
                else
                    DisplayError("CreateTemplate()", iError);
            }
            else
                DisplayError("GetImage()", iError);
        }
        private void pictureBoxR2_Load(Byte[] fp_image, Int32 img_qlty)
        {
            Int32 iError = 0;

            fp_image = new Byte[m_ImageWidth * m_ImageHeight];
            iError = m_FPM.GetImage(fp_image);

            m_FPM.GetImageQuality(m_ImageWidth, m_ImageHeight, fp_image, ref img_qlty);
            progressBar_R2.Value = img_qlty;

            if (iError == (Int32)SGFPMError.ERROR_NONE)
            {
                DrawImage(fp_image, pictureBoxR2);
                iError = m_FPM.CreateTemplate(fp_image, m_RegMin2);

                if (iError == (Int32)SGFPMError.ERROR_NONE)
                {
                    StatusBar.Text = "Chụp hình: Vân tay 2";
                    save_finger = CreatedTable(imageToByteArray(pictureBoxR1.Image), imageToByteArray(pictureBoxR2.Image), progressBar_R1.Value, progressBar_R2.Value, m_RegMin1, m_RegMin2);
                    count_finger++;
                }
                else
                    DisplayError("CreateTemplate()", iError);
            }
            else
                DisplayError("GetImage()", iError);

        }
        ///////////////////////
        private void DrawImage(Byte[] imgData, PictureBox picBox)
        {
            int colorval;
            Bitmap bmp = new Bitmap(m_ImageWidth, m_ImageHeight);
            picBox.Image = (Image)bmp;

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    colorval = (int)imgData[(j * m_ImageWidth) + i];
                    bmp.SetPixel(i, j, Color.FromArgb(colorval, colorval, colorval));
                }
            }
            picBox.Refresh();
        }

        ///////////////////////
        public byte[] imageToByteArray(Image imageIn)
        {

            using (var ms = new MemoryStream())
            {
                Bitmap bmp = new Bitmap(imageIn);
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
            }

            //MemoryStream ms = new MemoryStream();
            //imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            //return ms.ToArray();

        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            Image returnImage = null;
            using (MemoryStream ms = new MemoryStream(byteArrayIn))
            {
                returnImage = Image.FromStream(ms);
            }
            return returnImage;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            frm_KhachHang_Load(sender, e);
        }
        private void BtnRegister_Click(object sender, System.EventArgs e)
        {

            Int32 iError;
            bool matched = false;
            Int32 match_score = 0;
            SGFPMSecurityLevel secu_level;

            secu_level = (SGFPMSecurityLevel)comboBoxSecuLevel_R.SelectedIndex;
            iError = m_FPM.MatchTemplate(m_RegMin1, m_RegMin2, secu_level, ref matched);
            iError = m_FPM.GetMatchingScore(m_RegMin1, m_RegMin2, ref match_score);

            if (iError == (Int32)SGFPMError.ERROR_NONE)
            {
                if (matched)
                    StatusBar.Text = "Registration Success, Matching Score: " + match_score;
                else
                    StatusBar.Text = "Registration Failed";
            }
            else
                DisplayError("GetMatchingScore()", iError);

        }

        private void BtnVerify_Click(object sender, EventArgs e)
        {

            Int32 iError;
            bool matched1 = false;
            bool matched2 = false;
            SGFPMSecurityLevel secu_level;

            secu_level = (SGFPMSecurityLevel)comboBoxSecuLevel_V.SelectedIndex;

            if (save_finger.Rows.Count > 0)
            {
                foreach (DataRow dr in save_finger.Rows)
                {
                    iError = m_FPM.MatchTemplate((Byte[])dr["finger_1"], m_VrfMin, secu_level, ref matched1);
                    iError = m_FPM.MatchTemplate((Byte[])dr["finger_2"], m_VrfMin, secu_level, ref matched2);

                    if (iError == (Int32)SGFPMError.ERROR_NONE)
                    {
                        if (matched1 & matched2)
                        {
                            GetInfo_Customer((int)dr["CustomerID"]);
                            StatusBar.Text = "Vân tay này là của --> " + txtFullname.Text;
                            break;
                        }
                        else { StatusBar.Text = "Kiếm tra vân tay thất bại !"; }
                    }
                    else
                        DisplayError("MatchTemplate()", iError);
                }
            }
            else
            {
                StatusBar.Text = "Không có vân tay để kiểm tra !";
            }
        }
        private void GetInfo_Customer(int cusId)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    DataTable dt = db.SqlQueryWithResult("[dbo].[CUSTOMER_SELECT]", CommandType.StoredProcedure, new SqlParameter("@cusId", cusId));
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            cusId = remain_cusId = int.Parse(dr["CustomerID"].ToString());
                            txtBarcode.Text = dr["Barcode"].ToString();
                            txtFullname.Text = dr["FullName"].ToString();
                            txtCard.Text = dr["IdentityCard"].ToString();
                            dateBorn.Text = dr["BornDay"].ToString();
                            SetSex(int.Parse(dr["Sex"].ToString()));
                            txtPhone.Text = dr["Phone"].ToString();
                            txtAddress.Text = dr["Address"].ToString();
                            txtEmail.Text = dr["Email"].ToString();
                            txtHeight.Text = dr["Height"].ToString();
                            txtWidth.Text = dr["Width"].ToString();
                            txtNote.Text = dr["Note"].ToString();
                            if (!string.IsNullOrEmpty(dr["ImageCamera"].ToString()))
                            {
                                var data = (Byte[])dr["ImageCamera"];
                                pictureBox1.Image = byteArrayToImage(data);
                            }
                            else
                            {
                                pictureBox1.Image = Properties.Resources.no_image;
                            }
                            pictureBoxR1.Image = null;
                            pictureBoxR2.Image = null;
                            GetFinger_Customer(cusId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }

        private void btnCamera_Click(object sender, EventArgs e)
        {
            type_image = 1;
            if (remain_click == 0) pictureBox1.Image = Image.FromFile(@"camera.jpg"); if (remain_click == 1) pictureBox1.Image = Image.FromFile(@"camera_" + remain_click.ToString() + ".jpg");
            remain_click++;
        }

        private void BtnCapture3_Click(object sender, EventArgs e)
        {
            Int32 iError;
            Byte[] fp_image;
            Int32 img_qlty;

            fp_image = new Byte[m_ImageWidth * m_ImageHeight];
            img_qlty = 0;

            iError = m_FPM.GetImage(fp_image);

            m_FPM.GetImageQuality(m_ImageWidth, m_ImageHeight, fp_image, ref img_qlty);
            progressBar_V1.Value = img_qlty;

            if (iError == (Int32)SGFPMError.ERROR_NONE)
            {
                DrawImage(fp_image, pictureBoxV1);
                iError = m_FPM.CreateTemplate(null, fp_image, m_VrfMin);

                if (iError == (Int32)SGFPMError.ERROR_NONE)
                    StatusBar.Text = "Image for verification is captured";
                else
                    DisplayError("CreateTemplate()", iError);
            }
            else
                DisplayError("GetImage()", iError);
        }

        private void btnAutoCheck_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            BtnCapture3_Click(sender, e);

            if (pictureBoxV1.Image != null)
            {
                save_finger = GetFinger_Customer(0);
                BtnVerify_Click(sender, e);
            };
        }


        private void btnStopCheck_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }
    }
}
