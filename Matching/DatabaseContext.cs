﻿using Common.Core.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace Common.Core.Data
{
    public class DatabaseContext : IDisposable
    {

        private SqlConnection _sqlConnection = new SqlConnection("Data Source=DESKTOP-UGFPND9;Initial Catalog=GymsDemo;Integrated Security=True");
        private string _connectionString = "Data Source=DESKTOP-UGFPND9;Initial Catalog=GymsDemo;Integrated Security=True";

        public DatabaseContext()
        {
            beginTransaction();
        }

        public DatabaseContext(string consStr)
        {
            _connectionString = consStr;
            beginTransaction();
        }

        public bool IsConnected
        {
            get
            {
                return checkIsConnected();
            }
            private set { }
        }

        private bool checkIsConnected()
        {
            if (_connectionString == string.Empty)
            {
                _sqlConnection = new SqlConnection(_connectionString);
            }
            SqlConnection conn = new SqlConnection(_connectionString);
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                    return true;
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                conn.Close();
            }
            return false;
        }

        private void beginTransaction()
        {
            if (checkIsConnected())
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
            }
        }

        public bool SqlQuery(string sql, CommandType ct, params SqlParameter[] param)
        {
            bool result = false;
            using (SqlTransaction _sqlTransaction = _sqlConnection.BeginTransaction())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(sql, _sqlConnection, _sqlTransaction);
                    cmd.CommandType = ct;
                    cmd.CommandTimeout = 6000;
                    cmd.Parameters.Clear();
                    if (param != null)
                    {
                        foreach (SqlParameter p in param)
                        {
                            cmd.Parameters.Add(p);
                        }
                    }

                    cmd.ExecuteNonQuery();
                    _sqlTransaction.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    _sqlTransaction.Rollback();
                    Console.WriteLine(ex.Message);
                    result = false;
                }
            }
            return result;
        }

        public bool SqlQuery(string sql, CommandType ct, ref string err, params SqlParameter[] param)
        {
            bool result = false;
            using (SqlTransaction _sqlTransaction = _sqlConnection.BeginTransaction())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(sql, _sqlConnection, _sqlTransaction);
                    cmd.CommandType = ct;
                    cmd.CommandTimeout = 6000;
                    cmd.Parameters.Clear();
                    if (param != null)
                    {
                        foreach (SqlParameter p in param)
                        {
                            cmd.Parameters.Add(p);
                        }
                    }

                    cmd.ExecuteNonQuery();
                    _sqlTransaction.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    _sqlTransaction.Rollback();
                    err = ex.Message;
                    Console.WriteLine(ex.Message);
                    result = false;
                }
            }
            return result;
        }

        public DataTable SqlQueryWithResult(string sql, CommandType ct, ref string err, params SqlParameter[] param)
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand(sql, _sqlConnection);
                cmd.CommandType = ct;
                cmd.CommandTimeout = 6000;
                cmd.Parameters.Clear();
                if (param != null)
                {
                    foreach (SqlParameter p in param)
                        cmd.Parameters.Add(p);
                }

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                err = ex.Message;
                Console.WriteLine(ex.Message);
            }

            return dt;
        }

        public DataTable SqlQueryWithResult(string sql, CommandType ct, params SqlParameter[] param)
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand(sql, _sqlConnection);
                cmd.CommandType = ct;
                cmd.CommandTimeout = 6000;
                cmd.Parameters.Clear();
                if (param != null)
                {
                    foreach (SqlParameter p in param)
                        cmd.Parameters.Add(p);
                }

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return dt;
        }

        public T SqlQueryGetValue<T>(string sql, CommandType ct, params SqlParameter[] param) where T : class
        {
            DataTable dt = new DataTable();
            var result = new object();

            try
            {
                SqlCommand cmd = new SqlCommand(sql, _sqlConnection);
                cmd.CommandType = ct;
                cmd.CommandTimeout = 6000;
                cmd.Parameters.Clear();
                if (param != null)
                {
                    foreach (SqlParameter p in param)
                        cmd.Parameters.Add(p);
                }

                result = cmd.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return (T)Convert.ChangeType(result, typeof(T));
        }

        public SqlDataAdapter SqlQueryWithAdapter(string sql, CommandType ct, params SqlParameter[] param)
        {
            SqlDataAdapter da = null;
            try
            {
                SqlCommand cmd = new SqlCommand(sql, _sqlConnection);
                cmd.CommandType = ct;
                cmd.CommandTimeout = 6000;
                cmd.Parameters.Clear();
                if (param != null)
                {
                    foreach (SqlParameter p in param)
                        cmd.Parameters.Add(p);
                }

                da = new SqlDataAdapter(cmd);
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return da;
        }

        public bool ExecuteTableDirect(string tableName, DataTable data)
        {
            bool result = false;
            try
            {
                using (var sqlBulk = new SqlBulkCopy(_sqlConnection))
                {
                    try
                    {
                        sqlBulk.DestinationTableName = tableName;
                        sqlBulk.WriteToServer(data);
                    }
                    catch (Exception ex)
                    {
                        result = false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }

        public bool ExecuteTableDirect(string tableName, DataTable data, ref string err)
        {
            bool result = false;
            try
            {
                using (var sqlBulk = new SqlBulkCopy(_sqlConnection))
                {
                    try
                    {
                        sqlBulk.DestinationTableName = tableName;
                        sqlBulk.WriteToServer(data);
                    }
                    catch (Exception ex)
                    {
                        err = ex.Message;
                        result = false;
                    }
                }
            }
            catch (SqlException ex)
            {
                err = ex.Message;
                Console.WriteLine(ex.Message);
            }

            return result;
        }

        public void Dispose()
        {
            if (_sqlConnection != null)
            {
                if (_sqlConnection.State == ConnectionState.Open)
                    _sqlConnection.Close();
                _sqlConnection.Dispose();
            }
        }

        public bool BulkCopy(DataTable dt, string tableName, ref string err)
        {

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(_sqlConnection))
            {
                bulkCopy.DestinationTableName = tableName;
                try
                {
                    bulkCopy.WriteToServer(dt);
                    _sqlConnection.Close();
                    return true;
                }
                catch (SqlException ex)
                {
                    err = ex.Message;
                    Console.WriteLine(ex.Message);
                    return false;
                }
            }
        }
       
    }
}
