﻿using Common.Core.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Matching_cs
{
    public partial class frm_KhachHang : Form
    {
        public frm_KhachHang()
        {
            InitializeComponent();
        }

        private void frm_KhachHang_Load(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    DataTable dt = db.SqlQueryWithResult("[dbo].[CUSTOMER_SELECT]", CommandType.StoredProcedure);
                    if (dt.Rows.Count > 0)
                    {
                        MessageBox.Show("OK");
                    }
                    else
                    {
                        MessageBox.Show("Open connect");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }
    }
}
